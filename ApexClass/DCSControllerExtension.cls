public with sharing class {{ api_name }} {
/**
*   {Purpose}  �  TODO:  Provide Purpose
*
*   {Contact}   - support@demandchainsystems.com
*                 www.demandchainsystems.com
*                 612-424-0032                  
*/

/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   YYYYMMDD  	FN LN DCS				Created
*   =============================================================================
*/
	private final sObject soObject;

	///////////////////////////
	// Constructors
	///////////////////////////
	public {{ api_name }}(ApexPages.StandardController scController) {
		// TODO
		this.soObject = (sObject)scController.getRecord();
	}

	public {{ api_name }}(ApexPages.StandardSetController sscController) {
		// TODO
	}
	
	
	///////////////////////////
	// GET / SET
	///////////////////////////
	public String recordName {
		get {
        	return 'Hello ' + (String)soObject.get('name') + ' (' + (Id)soObject.get('Id') + ')';
        }
        set { recordName = value; }
    }
	
	///////////////////////////
	// Action Methods
	///////////////////////////
	
	
	///////////////////////////
	// Inner / Helper Classes
	///////////////////////////
}