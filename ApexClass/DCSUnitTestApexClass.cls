/**
*   {Purpose}  �  TODO:  Provide Purpose
*
*	{Code Covered}	- Note the triggers/classes that this test covers
*
*   {Contact}   - support@demandchainsystems.com
*                 www.demandchainsystems.com
*                 612-424-0032                  
*/

/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   YYYYMMDD  	FN LN DCS				Created
*   =============================================================================
*/
@isTest
private class {{ api_name }} {
	
	@isTest static void testController() {
		
		// Implement test code

		/************************************

			- Remember to use test class data generators
			- Remember to liberally use System.Asserts!
			- DCS strives for 100% test coverage!
			
		************************************/

	}
	
}