public with sharing class {{ api_name }} {
/**
*   {Purpose}  �  TODO:  Provide Purpose
*
*   {Contact}   - support@demandchainsystems.com
*                 www.demandchainsystems.com
*                 612-424-0032                  
*/

/**
*   CHANGE  HISTORY
*   =============================================================================
*   Date    	Name             		Description
*   YYYYMMDD  	FN LN DCS				Created
*   =============================================================================
*/


	///////////////////////////
	// Constructors
	///////////////////////////
	public {{ api_name }}() {
		
	}
	
	
	///////////////////////////
	// GET / SET
	///////////////////////////
	
	
	///////////////////////////
	// Action Methods
	///////////////////////////
	
	
	///////////////////////////
	// Inner / Helper Classes
	///////////////////////////
}